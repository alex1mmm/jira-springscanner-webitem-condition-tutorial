package ru.matveev.alexey.atlassian.jira.condition.tutorial.api;

public interface MyPluginComponent
{
    String getName();
}